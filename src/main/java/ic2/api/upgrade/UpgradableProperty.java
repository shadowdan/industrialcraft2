// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.upgrade;

public enum UpgradableProperty
{
    Processing, 
    Augmentable, 
    RedstoneSensitive, 
    Transformer, 
    EnergyStorage, 
    ItemConsuming, 
    ItemProducing, 
    FluidConsuming, 
    FluidProducing;
}
