// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.upgrade;

public interface IFullUpgrade extends IAugmentationUpgrade, IEnergyStorageUpgrade, IFluidConsumingUpgrade, IFluidProducingUpgrade, IItemConsumingUpgrade, IItemProducingUpgrade, IProcessingUpgrade, IRedstoneSensitiveUpgrade, ITransformerUpgrade
{
}
