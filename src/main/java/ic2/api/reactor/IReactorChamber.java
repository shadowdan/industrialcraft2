// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.reactor;

public interface IReactorChamber
{
    IReactor getReactorInstance();
    
    boolean isWall();
}
