// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.reactor;

import net.minecraft.item.ItemStack;

public interface IBaseReactorComponent
{
    boolean canBePlacedIn(final ItemStack p0, final IReactor p1);
}
