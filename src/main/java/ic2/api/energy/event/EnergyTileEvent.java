// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.energy.event;

import ic2.api.energy.EnergyNet;
import ic2.api.energy.tile.IEnergyTile;
import net.minecraftforge.event.world.WorldEvent;

public class EnergyTileEvent extends WorldEvent
{
    public final IEnergyTile tile;
    
    public EnergyTileEvent(final IEnergyTile tile) {
        super(EnergyNet.instance.getWorld(tile));
        if (this.getWorld() == null) {
            throw new NullPointerException("world is null");
        }
        this.tile = tile;
    }
}
