// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.energy.tile;

public interface IDischargingSlot
{
    double discharge(final double p0, final boolean p1);
}
