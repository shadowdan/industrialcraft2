// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.item;

public interface IDebuggable
{
    boolean isDebuggable();
    
    String getDebugText();
}
