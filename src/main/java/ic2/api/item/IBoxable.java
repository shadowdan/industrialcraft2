// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.item;

import net.minecraft.item.ItemStack;

public interface IBoxable
{
    boolean canBeStoredInToolbox(final ItemStack p0);
}
