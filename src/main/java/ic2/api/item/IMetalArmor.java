// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface IMetalArmor
{
    boolean isMetalArmor(final ItemStack p0, final EntityPlayer p1);
}
