// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.item;

import net.minecraft.item.ItemStack;

public interface ISpecialElectricItem
{
    IElectricItemManager getManager(final ItemStack p0);
}
