// 
// Decompiled by Procyon v0.5.30
// 

package ic2.api.item;

import java.util.List;
import net.minecraft.item.ItemStack;

public interface IItemHudInfo
{
    List<String> getHudInfo(final ItemStack p0, final boolean p1);
}
