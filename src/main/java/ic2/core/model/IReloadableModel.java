// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.model;

import net.minecraftforge.client.model.IModel;

public interface IReloadableModel extends IModel
{
    void onReload();
}
