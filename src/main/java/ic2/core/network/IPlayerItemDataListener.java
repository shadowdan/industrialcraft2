// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.network;

import net.minecraft.entity.player.EntityPlayer;

public interface IPlayerItemDataListener
{
    void onPlayerItemNetworkData(final EntityPlayer p0, final int p1, final Object... p2);
}
