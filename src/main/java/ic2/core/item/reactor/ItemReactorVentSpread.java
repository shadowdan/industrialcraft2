// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.item.reactor;

import ic2.api.reactor.IReactorComponent;
import ic2.api.reactor.IReactor;
import net.minecraft.item.ItemStack;
import ic2.core.ref.ItemName;

public class ItemReactorVentSpread extends AbstractReactorComponent
{
    public final int sideVent;
    
    public ItemReactorVentSpread(final ItemName name, final int sidevent) {
        super(name);
        this.sideVent = sidevent;
    }
    
    @Override
    public void processChamber(final ItemStack stack, final IReactor reactor, final int x, final int y, final boolean heatrun) {
        if (heatrun) {
            this.cool(reactor, x - 1, y);
            this.cool(reactor, x + 1, y);
            this.cool(reactor, x, y - 1);
            this.cool(reactor, x, y + 1);
        }
    }
    
    private void cool(final IReactor reactor, final int x, final int y) {
        final ItemStack stack = reactor.getItemAt(x, y);
        if (stack != null && stack.getItem() instanceof IReactorComponent) {
            final IReactorComponent comp = (IReactorComponent)stack.getItem();
            if (comp.canStoreHeat(stack, reactor, x, y)) {
                final int self = comp.alterHeat(stack, reactor, x, y, -this.sideVent);
                if (self <= 0) {
                    reactor.addEmitHeat(self + this.sideVent);
                }
            }
        }
    }
}
