// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.item.type;

public interface IRadioactiveItemType
{
    int getRadiationDuration();
    
    int getRadiationAmplifier();
}
