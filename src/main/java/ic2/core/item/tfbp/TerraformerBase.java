// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.item.tfbp;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

abstract class TerraformerBase
{
    abstract boolean terraform(final World p0, final BlockPos p1);
    
    void init() {
    }
}
