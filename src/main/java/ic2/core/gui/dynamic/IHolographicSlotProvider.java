// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.gui.dynamic;

import net.minecraft.item.ItemStack;

public interface IHolographicSlotProvider
{
    ItemStack[] getStacksForName(final String p0);
}
