// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.gui;

public interface IOverlaySupplier
{
    int getUS();
    
    int getVS();
    
    int getUE();
    
    int getVE();
}
