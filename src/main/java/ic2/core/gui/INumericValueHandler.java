// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.gui;

public interface INumericValueHandler
{
    int getValue();
    
    void onChange(final int p0);
}
