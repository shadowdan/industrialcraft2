// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.audio;

import net.minecraft.entity.player.EntityPlayer;

public class AudioSource
{
    public void remove() {
    }
    
    public void play() {
    }
    
    public void pause() {
    }
    
    public void stop() {
    }
    
    public void flush() {
    }
    
    public float getVolume() {
        return 0.0f;
    }
    
    public void setVolume(final float volume) {
    }
    
    public void setPitch(final float pitch) {
    }
    
    public void updatePosition() {
    }
    
    public void activate() {
    }
    
    public void cull() {
    }
    
    public void updateVolume(final EntityPlayer player) {
    }
    
    public float getRealVolume() {
        return 0.0f;
    }
}
