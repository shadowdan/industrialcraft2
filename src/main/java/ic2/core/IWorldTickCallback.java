// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core;

import net.minecraft.world.World;

public interface IWorldTickCallback
{
    void onTick(final World p0);
}
