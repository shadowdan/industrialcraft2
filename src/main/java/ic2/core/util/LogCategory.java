// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.util;

public enum LogCategory
{
    General, 
    Armor, 
    Audio, 
    Block, 
    Component, 
    EnergyNet, 
    Item, 
    Network, 
    PlayerActivity, 
    Recipe, 
    Resource, 
    SubModule, 
    Uu, 
    WorldGen;
}
