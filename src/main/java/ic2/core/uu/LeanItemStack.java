// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.uu;

import ic2.core.util.StackUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.item.Item;

class LeanItemStack
{
    private final Item item;
    private final int meta;
    private final NBTTagCompound nbt;
    private final int size;
    private int hashCode;
    
    public LeanItemStack(final ItemStack stack) {
        this(stack.getItem(), StackUtil.getRawMeta(stack), stack.getTagCompound(), StackUtil.getSize(stack));
    }
    
    public LeanItemStack(final ItemStack stack, final int size) {
        this(stack.getItem(), StackUtil.getRawMeta(stack), stack.getTagCompound(), size);
    }
    
    public LeanItemStack(final Item item, final int meta, final NBTTagCompound nbt, final int size) {
        if (item == null) {
            throw new NullPointerException("null item");
        }
        this.item = item;
        this.meta = meta;
        this.nbt = nbt;
        this.size = size;
    }
    
    public Item getItem() {
        return this.item;
    }
    
    public int getMeta() {
        return this.meta;
    }
    
    public NBTTagCompound getNbt() {
        return this.nbt;
    }
    
    public int getSize() {
        return this.size;
    }
    
    @Override
    public String toString() {
        return String.format("%dx%s@%d", this.size, this.item.getRegistryName(), this.meta);
    }
    
    public boolean hasSameItem(final LeanItemStack o) {
        return this.item == o.item && (this.meta == o.meta || !this.item.getHasSubtypes()) && StackUtil.checkNbtEquality(this.nbt, o.nbt);
    }
    
    public LeanItemStack copy() {
        return this.copyWithSize(this.size);
    }
    
    public LeanItemStack copyWithSize(final int newSize) {
        final LeanItemStack ret = new LeanItemStack(this.item, this.meta, this.nbt, newSize);
        ret.hashCode = this.hashCode;
        return ret;
    }
    
    public ItemStack toMcStack() {
        if (this.size <= 0) {
            return StackUtil.emptyStack;
        }
        final ItemStack ret = new ItemStack(this.item, this.size, this.meta);
        ret.setTagCompound(this.nbt);
        return ret;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof LeanItemStack)) {
            return false;
        }
        final LeanItemStack o = (LeanItemStack)obj;
        return this.item == o.item && this.meta == o.meta && ((this.nbt == null && o.nbt == null) || (this.nbt != null && o.nbt != null && this.nbt.equals((Object)o.nbt)));
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            this.hashCode = this.calculateHashCode();
        }
        return this.hashCode;
    }
    
    private int calculateHashCode() {
        int ret = System.identityHashCode(this.item);
        ret = ret * 31 + this.meta;
        if (this.nbt != null) {
            ret = ret * 61 + this.nbt.hashCode();
        }
        if (ret == 0) {
            ret = -1;
        }
        return ret;
    }
}
