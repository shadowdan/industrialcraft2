// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.uu;

import java.util.List;

public interface IRecipeResolver
{
    List<RecipeTransformation> getTransformations();
}
