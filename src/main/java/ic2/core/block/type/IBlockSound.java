// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.block.type;

import net.minecraft.block.SoundType;

public interface IBlockSound
{
    SoundType getSound();
}
