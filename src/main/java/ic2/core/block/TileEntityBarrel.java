// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.block;

import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import ic2.core.ref.BlockName;
import net.minecraftforge.fluids.FluidStack;
import net.minecraft.item.ItemStack;
import ic2.core.item.type.CropResItemType;
import net.minecraft.init.Items;
import ic2.core.util.LiquidUtil;
import ic2.api.util.FluidContainerOutputMode;
import net.minecraftforge.fluids.FluidRegistry;
import ic2.core.ref.ItemName;
import ic2.core.util.StackUtil;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import ic2.core.item.ItemBooze;

public class TileEntityBarrel extends TileEntityBlock
{
    private int type;
    private int boozeAmount;
    private int age;
    private boolean opened;
    private byte hopsCount;
    private byte wheatCount;
    private byte solidRatio;
    private byte hopsRatio;
    private byte timeRatio;
    
    public TileEntityBarrel() {
        this.type = 0;
        this.boozeAmount = 0;
        this.age = 0;
        this.hopsCount = 0;
        this.wheatCount = 0;
        this.solidRatio = 0;
        this.hopsRatio = 0;
        this.timeRatio = 0;
    }
    
    public TileEntityBarrel(final int value) {
        this.type = 0;
        this.boozeAmount = 0;
        this.age = 0;
        this.hopsCount = 0;
        this.wheatCount = 0;
        this.solidRatio = 0;
        this.hopsRatio = 0;
        this.timeRatio = 0;
        this.type = ItemBooze.getTypeOfValue(value);
        if (this.type > 0) {
            this.boozeAmount = ItemBooze.getAmountOfValue(value);
        }
        if (this.type == 1) {
            this.opened = true;
            this.hopsRatio = (byte)ItemBooze.getHopsRatioOfBeerValue(value);
            this.solidRatio = (byte)ItemBooze.getSolidRatioOfBeerValue(value);
            this.timeRatio = (byte)ItemBooze.getTimeRatioOfBeerValue(value);
        }
        if (this.type == 2) {
            this.opened = false;
            this.age = this.timeNedForRum(this.boozeAmount) * ItemBooze.getProgressOfRumValue(value) / 100;
        }
    }
    
    @Override
    public void readFromNBT(final NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.type = nbt.getByte("type");
        this.boozeAmount = nbt.getByte("waterCount");
        this.age = nbt.getInteger("age");
        this.opened = nbt.getBoolean("opened");
        if (this.type == 1) {
            if (!this.opened) {
                this.hopsCount = nbt.getByte("hopsCount");
                this.wheatCount = nbt.getByte("wheatCount");
            }
            this.solidRatio = nbt.getByte("solidRatio");
            this.hopsRatio = nbt.getByte("hopsRatio");
            this.timeRatio = nbt.getByte("timeRatio");
        }
    }
    
    @Override
    public NBTTagCompound writeToNBT(final NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setByte("type", (byte)this.type);
        nbt.setByte("waterCount", (byte)this.boozeAmount);
        nbt.setInteger("age", this.age);
        nbt.setBoolean("opened", this.opened);
        if (this.type == 1) {
            if (!this.opened) {
                nbt.setByte("hopsCount", this.hopsCount);
                nbt.setByte("wheatCount", this.wheatCount);
            }
            nbt.setByte("solidRatio", this.solidRatio);
            nbt.setByte("hopsRatio", this.hopsRatio);
            nbt.setByte("timeRatio", this.timeRatio);
        }
        return nbt;
    }
    
    @Override
    protected void updateEntityServer() {
        super.updateEntityServer();
        if (!this.isEmpty() && !this.getActive()) {
            ++this.age;
            if (this.type == 1 && this.timeRatio < 5) {
                int x = this.timeRatio;
                if (x == 4) {
                    x += 2;
                }
                if (this.age >= 24000.0 * Math.pow(3.0, x)) {
                    this.age = 0;
                    ++this.timeRatio;
                }
            }
        }
    }
    
    public boolean isEmpty() {
        return this.type == 0 || this.boozeAmount <= 0;
    }
    
    @Override
    protected boolean onActivated(final EntityPlayer player, final EnumHand hand, final EnumFacing side, final float hitX, final float hitY, final float hitZ) {
        final ItemStack stack = StackUtil.get(player, hand);
        if (stack == null) {
            return false;
        }
        if (side.getAxis() != EnumFacing.Axis.Y && !this.getActive() && StackUtil.consume(player, hand, StackUtil.sameStack(ItemName.treetap.getItemStack()), 1)) {
            if (!this.getWorld().isRemote) {
                this.setActive(true);
            }
            if (this.getFacing() != side) {
                this.setFacing(side);
            }
            return true;
        }
        if (!this.opened) {
            if (this.type == 0 || this.type == 1) {
                final int minAmount = 1000;
                int space = (32 - this.boozeAmount) * 1000;
                if (player.isSneaking()) {
                    space = Math.min(space, 1000);
                }
                FluidStack fs;
                if (space >= 1000 && (fs = LiquidUtil.drainContainer(player, hand, FluidRegistry.WATER, space, FluidContainerOutputMode.InPlacePreferred, true)) != null && fs.amount >= 1000) {
                    final int amount = fs.amount / 1000 * 1000;
                    fs = LiquidUtil.drainContainer(player, hand, FluidRegistry.WATER, amount, FluidContainerOutputMode.InPlacePreferred, true);
                    if (fs.amount != amount) {
                        return false;
                    }
                    LiquidUtil.drainContainer(player, hand, FluidRegistry.WATER, amount, FluidContainerOutputMode.InPlacePreferred, false);
                    this.type = 1;
                    this.boozeAmount += amount / 1000;
                    return true;
                }
                else if (stack.getItem() == Items.WHEAT) {
                    this.type = 1;
                    int amount = StackUtil.getSize(stack);
                    if (player.isSneaking()) {
                        amount = 1;
                    }
                    if (amount > 64 - this.wheatCount) {
                        amount = 64 - this.wheatCount;
                    }
                    if (amount <= 0) {
                        return false;
                    }
                    this.wheatCount += (byte)amount;
                    StackUtil.consumeOrError(player, hand, amount);
                    this.alterComposition();
                    return true;
                }
                else if (StackUtil.checkItemEquality(stack, ItemName.crop_res.getItemStack(CropResItemType.hops))) {
                    this.type = 1;
                    int amount = StackUtil.getSize(stack);
                    if (player.isSneaking()) {
                        amount = 1;
                    }
                    if (amount > 64 - this.hopsCount) {
                        amount = 64 - this.hopsCount;
                    }
                    if (amount <= 0) {
                        return false;
                    }
                    this.hopsCount += (byte)amount;
                    StackUtil.consumeOrError(player, hand, amount);
                    this.alterComposition();
                    return true;
                }
            }
            if ((this.type == 0 || this.type == 2) && stack.getItem() == Items.REEDS) {
                if (this.age > 600) {
                    return false;
                }
                this.type = 2;
                int amount2 = StackUtil.getSize(stack);
                if (player.isSneaking()) {
                    amount2 = 1;
                }
                if (this.boozeAmount + amount2 > 32) {
                    amount2 = 32 - this.boozeAmount;
                }
                if (amount2 <= 0) {
                    return false;
                }
                this.boozeAmount += amount2;
                StackUtil.consumeOrError(player, hand, amount2);
                return true;
            }
        }
        return false;
    }
    
    @Override
    protected void onClicked(final EntityPlayer player) {
        super.onClicked(player);
        final World world = this.getWorld();
        if (this.getActive()) {
            if (!world.isRemote) {
                StackUtil.dropAsEntity(world, this.pos, ItemName.treetap.getItemStack());
                this.setActive(false);
            }
            this.drainLiquid(1);
            return;
        }
        if (!world.isRemote) {
            StackUtil.dropAsEntity(world, this.pos, new ItemStack(ItemName.barrel.getInstance(), 1, this.calculateMetaValue()));
        }
        world.setBlockState(this.pos, BlockName.scaffold.getBlockState(BlockScaffold.ScaffoldType.wood));
    }
    
    private void alterComposition() {
        if (this.timeRatio <= 0) {
            this.age = 0;
        }
        else if (this.timeRatio == 1) {
            final World world = this.getWorld();
            if (world.rand.nextBoolean()) {
                this.timeRatio = 0;
            }
            else if (world.rand.nextBoolean()) {
                this.timeRatio = 5;
            }
        }
        else if (this.timeRatio == 2) {
            if (this.getWorld().rand.nextBoolean()) {
                this.timeRatio = 5;
            }
        }
        else {
            this.timeRatio = 5;
        }
    }
    
    public boolean drainLiquid(final int amount) {
        if (this.isEmpty()) {
            return false;
        }
        if (amount > this.boozeAmount) {
            return false;
        }
        this.open();
        if (this.type == 2) {
            final int progress = this.age * 100 / this.timeNedForRum(this.boozeAmount);
            this.boozeAmount -= amount;
            this.age = progress / 100 * this.timeNedForRum(this.boozeAmount);
        }
        else {
            this.boozeAmount -= amount;
        }
        if (this.boozeAmount <= 0) {
            if (this.type == 1) {
                this.hopsCount = 0;
                this.wheatCount = 0;
                this.hopsRatio = 0;
                this.solidRatio = 0;
                this.timeRatio = 0;
            }
            this.type = 0;
            this.opened = false;
            this.boozeAmount = 0;
        }
        return true;
    }
    
    private void open() {
        if (this.opened) {
            return;
        }
        this.opened = true;
        if (this.type == 1) {
            float ratio;
            if (this.hopsCount <= 0) {
                ratio = 0.0f;
            }
            else {
                ratio = this.hopsCount / this.wheatCount;
            }
            if (ratio <= 0.25f) {
                this.hopsRatio = 0;
            }
            else if (ratio <= 0.33333334f) {
                this.hopsRatio = 1;
            }
            else if (ratio <= 0.5f) {
                this.hopsRatio = 2;
            }
            else if (ratio < 2.0f) {
                this.hopsRatio = 3;
            }
            else {
                this.hopsRatio = (byte)Math.min(6.0, Math.floor(ratio) + 2.0);
                if (ratio >= 5.0f) {
                    this.timeRatio = 5;
                }
            }
            if (this.boozeAmount <= 0) {
                ratio = Float.POSITIVE_INFINITY;
            }
            else {
                ratio = (this.hopsCount + this.wheatCount) / this.boozeAmount;
            }
            if (ratio <= 0.41666666f) {
                this.solidRatio = 0;
            }
            else if (ratio <= 0.5f) {
                this.solidRatio = 1;
            }
            else if (ratio < 1.0f) {
                this.solidRatio = 2;
            }
            else if (ratio == 1.0f) {
                this.solidRatio = 3;
            }
            else if (ratio < 2.0f) {
                this.solidRatio = 4;
            }
            else if (ratio < 2.4f) {
                this.solidRatio = 5;
            }
            else {
                this.solidRatio = 6;
                if (ratio >= 4.0f) {
                    this.timeRatio = 5;
                }
            }
        }
    }
    
    public int calculateMetaValue() {
        if (this.isEmpty()) {
            return 0;
        }
        if (this.type == 1) {
            this.open();
            int value = 0;
            value |= this.timeRatio;
            value <<= 3;
            value |= this.hopsRatio;
            value <<= 3;
            value |= this.solidRatio;
            value <<= 5;
            value |= this.boozeAmount - 1;
            value <<= 2;
            value |= this.type;
            return value;
        }
        if (this.type == 2) {
            this.open();
            int value = 0;
            int progress = this.age * 100 / this.timeNedForRum(this.boozeAmount);
            if (progress > 100) {
                progress = 100;
            }
            value |= progress;
            value <<= 5;
            value |= this.boozeAmount - 1;
            value <<= 2;
            value |= this.type;
            return value;
        }
        return 0;
    }
    
    public int timeNedForRum(final int amount) {
        return (int)(1200 * amount * Math.pow(0.95, amount - 1));
    }
    
    @Override
    protected ItemStack getPickBlock(final EntityPlayer player, final RayTraceResult target) {
        return BlockName.scaffold.getItemStack(BlockScaffold.ScaffoldType.wood);
    }
    
    @Override
    protected List<ItemStack> getAuxDrops(final int fortune) {
        final List<ItemStack> ret = new ArrayList<ItemStack>(super.getAuxDrops(fortune));
        ret.add(ItemName.barrel.getItemStack());
        return ret;
    }
}
