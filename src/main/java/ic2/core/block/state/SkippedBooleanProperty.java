// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.block.state;

import net.minecraft.block.properties.PropertyBool;

public class SkippedBooleanProperty extends PropertyBool implements ISkippableProperty
{
    public SkippedBooleanProperty(final String name) {
        super(name);
    }
}
