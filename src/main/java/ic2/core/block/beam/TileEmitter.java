// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.block.beam;

import net.minecraft.entity.Entity;
import ic2.core.block.machine.tileentity.TileEntityElectricMachine;

public class TileEmitter extends TileEntityElectricMachine
{
    private int progress;
    
    public TileEmitter() {
        super(5000, 1);
    }
    
    @Override
    protected void updateEntityServer() {
        super.updateEntityServer();
        if (this.progress < 100) {
            ++this.progress;
        }
        if (this.progress == 100 && this.getWorld().isBlockPowered(this.pos)) {
            this.progress = 0;
            this.getWorld().spawnEntity((Entity)new EntityParticle(this));
        }
    }
}
