// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.block.wiring;

enum CableFoam
{
    None, 
    Soft, 
    Hardened;
    
    public static final CableFoam[] values;
    
    static {
        values = values();
    }
}
