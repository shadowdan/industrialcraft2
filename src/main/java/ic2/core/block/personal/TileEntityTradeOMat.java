// 
// Decompiled by Procyon v0.5.30
// 

package ic2.core.block.personal;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiScreen;
import ic2.core.ContainerBase;
import net.minecraft.entity.Entity;
import net.minecraft.inventory.IInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ic2.core.network.NetworkManager;
import ic2.core.util.Util;
import ic2.core.util.LogCategory;
import net.minecraft.tileentity.TileEntity;
import ic2.core.util.StackUtil;
import java.util.List;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.nbt.NBTTagCompound;
import ic2.core.IC2;
import ic2.core.block.invslot.InvSlotOutput;
import ic2.core.block.invslot.InvSlotConsumableLinked;
import ic2.core.block.invslot.InvSlot;
import com.mojang.authlib.GameProfile;
import ic2.api.network.INetworkClientTileEntityEventListener;
import ic2.api.network.INetworkTileEntityEventListener;
import ic2.core.IHasGui;
import ic2.core.block.TileEntityInventory;

public class TileEntityTradeOMat extends TileEntityInventory implements IPersonalBlock, IHasGui, INetworkTileEntityEventListener, INetworkClientTileEntityEventListener
{
    private int ticker;
    private GameProfile owner;
    public int totalTradeCount;
    public int stock;
    public boolean infinite;
    private static final int stockUpdateRate = 64;
    private static final int EventTrade = 0;
    public final InvSlot demandSlot;
    public final InvSlot offerSlot;
    public final InvSlotConsumableLinked inputSlot;
    public final InvSlotOutput outputSlot;
    
    public TileEntityTradeOMat() {
        this.owner = null;
        this.totalTradeCount = 0;
        this.stock = 0;
        this.infinite = false;
        this.ticker = IC2.random.nextInt(64);
        this.demandSlot = new InvSlot(this, "demand", InvSlot.Access.NONE, 1);
        this.offerSlot = new InvSlot(this, "offer", InvSlot.Access.NONE, 1);
        this.inputSlot = new InvSlotConsumableLinked(this, "input", 1, this.demandSlot);
        this.outputSlot = new InvSlotOutput(this, "output", 1);
    }
    
    @Override
    public void readFromNBT(final NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        if (nbt.hasKey("ownerGameProfile")) {
            this.owner = NBTUtil.readGameProfileFromNBT(nbt.getCompoundTag("ownerGameProfile"));
        }
        this.totalTradeCount = nbt.getInteger("totalTradeCount");
        if (nbt.hasKey("infinite")) {
            this.infinite = nbt.getBoolean("infinite");
        }
    }
    
    @Override
    public NBTTagCompound writeToNBT(final NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.owner != null) {
            final NBTTagCompound ownerNbt = new NBTTagCompound();
            NBTUtil.writeGameProfile(ownerNbt, this.owner);
            nbt.setTag("ownerGameProfile", (NBTBase)ownerNbt);
        }
        nbt.setInteger("totalTradeCount", this.totalTradeCount);
        if (this.infinite) {
            nbt.setBoolean("infinite", this.infinite);
        }
        return nbt;
    }
    
    @Override
    public List<String> getNetworkedFields() {
        final List<String> ret = super.getNetworkedFields();
        ret.add("owner");
        return ret;
    }
    
    @Override
    protected void updateEntityServer() {
        super.updateEntityServer();
        this.trade();
        if (this.infinite) {
            this.stock = -1;
        }
        else if (++this.ticker % 64 == 0) {
            this.updateStock();
        }
    }
    
    private void trade() {
        final ItemStack tradedIn = this.inputSlot.consumeLinked(true);
        if (StackUtil.isEmpty(tradedIn)) {
            return;
        }
        final ItemStack offer = this.offerSlot.get();
        if (StackUtil.isEmpty(offer)) {
            return;
        }
        if (!this.outputSlot.canAdd(offer)) {
            return;
        }
        if (this.infinite) {
            this.inputSlot.consumeLinked(false);
            this.outputSlot.add(offer);
        }
        else {
            int amount = StackUtil.fetch(this, offer, true);
            if (amount != StackUtil.getSize(offer)) {
                return;
            }
            final int transferredOut = StackUtil.distribute(this, tradedIn, true);
            if (transferredOut != StackUtil.getSize(tradedIn)) {
                return;
            }
            amount = StackUtil.fetch(this, offer, false);
            if (amount == 0) {
                return;
            }
            if (amount != StackUtil.getSize(offer)) {
                IC2.log.warn(LogCategory.Block, "The Trade-O-Mat at %s received an inconsistent result from an adjacent trade supply inventory, the %s items will be lost.", Util.formatPosition(this), amount);
                return;
            }
            StackUtil.distribute(this, this.inputSlot.consumeLinked(false), false);
            this.outputSlot.add(offer);
            --this.stock;
        }
        ++this.totalTradeCount;
        IC2.network.get(true).initiateTileEntityEvent(this, 0, true);
        this.markDirty();
    }
    
    @Override
    protected void onLoaded() {
        super.onLoaded();
        if (IC2.platform.isSimulating()) {
            this.updateStock();
        }
    }
    
    public void updateStock() {
        final ItemStack offer = this.offerSlot.get();
        if (StackUtil.isEmpty(offer)) {
            this.stock = 0;
        }
        else {
            this.stock = StackUtil.fetch(this, StackUtil.copyWithSize(offer, Integer.MAX_VALUE), true) / StackUtil.getSize(offer);
        }
    }
    
    public boolean wrenchCanRemove(final EntityPlayer player) {
        return this.permitsAccess(player.getGameProfile());
    }
    
    @Override
    public boolean permitsAccess(final GameProfile profile) {
        return TileEntityPersonalChest.checkAccess(this, profile);
    }
    
    @Override
    public IInventory getPrivilegedInventory(final GameProfile accessor) {
        return (IInventory)this;
    }
    
    @Override
    public GameProfile getOwner() {
        return this.owner;
    }
    
    @Override
    public void setOwner(final GameProfile owner) {
        this.owner = owner;
    }
    
    @Override
    protected boolean canEntityDestroy(final Entity entity) {
        return false;
    }
    
    @Override
    public ContainerBase<TileEntityTradeOMat> getGuiContainer(final EntityPlayer player) {
        if (this.permitsAccess(player.getGameProfile())) {
            return new ContainerTradeOMatOpen(player, this);
        }
        return new ContainerTradeOMatClosed(player, this);
    }
    
    @SideOnly(Side.CLIENT)
    @Override
    public GuiScreen getGui(final EntityPlayer player, final boolean isAdmin) {
        if (isAdmin || this.permitsAccess(player.getGameProfile())) {
            return (GuiScreen)new GuiTradeOMatOpen(new ContainerTradeOMatOpen(player, this), isAdmin);
        }
        return (GuiScreen)new GuiTradeOMatClosed(new ContainerTradeOMatClosed(player, this));
    }
    
    @Override
    public void onGuiClosed(final EntityPlayer player) {
    }
    
    @Override
    public void onNetworkEvent(final int event) {
        switch (event) {
            case 0: {
                IC2.audioManager.playOnce(this, "Machines/o-mat.ogg");
                break;
            }
            default: {
                IC2.platform.displayError("An unknown event type was received over multiplayer.\nThis could happen due to corrupted data or a bug.\n\n(Technical information: event ID " + event + ", tile entity below)\nT: " + this + " (" + this.pos + ")", new Object[0]);
                break;
            }
        }
    }
    
    @Override
    public void onNetworkEvent(final EntityPlayer player, final int event) {
        if (event == 0 && this.getWorld().getMinecraftServer().getPlayerList().canSendCommands(player.getGameProfile()) && !(this.infinite = !this.infinite)) {
            this.updateStock();
        }
    }
}
